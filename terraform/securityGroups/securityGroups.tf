variable ingress_cidr_blocks {
  type = "list"
}

variable egress_cidr_blocks {
  type = "list"
}

variable name {}
variable tag_name {}
variable tag_app_name {}
variable tag_env {}
variable description {}
variable vpc_id {}

variable "ingress_from_port" {
  default = "0"
}

variable "ingress_to_port" {
  default = "0"
}

variable "inress_protocol" {
  default = "-1"
}

variable "egress_from_port" {
  default = "0"
}

variable "egress_to_port" {
  default = "0"
}
variable "egress_protocol" {
  default = "-1"
}


resource "aws_security_group" "security_group" {
  name        = "${var.name}"
  description = "${var.description}"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = "${var.ingress_from_port}"
    to_port     = "${var.ingress_to_port}"
    protocol    = "${var.inress_protocol}"
    cidr_blocks = ["${var.ingress_cidr_blocks}"]
  }

  egress {
    from_port   = "${var.egress_from_port}"
    to_port     = "${var.egress_to_port}"
    protocol    = "${var.egress_protocol}"
    cidr_blocks = ["${var.egress_cidr_blocks}"]
  }

  tags {
    Name = "${var.tag_name}"
    ApplicationName = "${var.tag_app_name}"
    ApplicationEnvironment = "${var.tag_env}"
  }
}

output "arn" {
  value = "${aws_security_group.security_group.arn}"
}

output "id" {
  value = "${aws_security_group.security_group.id}"
}
