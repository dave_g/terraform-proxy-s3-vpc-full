variable protocol {
  default = "-1"
}
variable from_port {
  default = "0"
}
variable to_port {
  default = "0"
}

variable egress {
  default = false
}
variable "rule_action" {
  default = "allow"
}
variable "cidr_blocks" {
  type = "list"
}
variable rule_number_seed {
  default = 100
}

variable "network_acl_id" {
  default = ""
}

locals {
  cidr_count = "${length(var.cidr_blocks)}"
}

resource "aws_network_acl_rule" "all-local-networks" {
  count = "${local.cidr_count}"
  network_acl_id = "${var.network_acl_id}"
  rule_number    = "${var.rule_number_seed + count.index}"
  egress         = "${var.egress}"
  protocol       = "${var.protocol}"
  rule_action    = "${var.rule_action}"
  cidr_block     = "${var.cidr_blocks[count.index]}"
  from_port      = "${var.from_port}"
  to_port        = "${var.to_port}"
}
/*
resource "aws_network_acl_rule" "all-local-networks" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 100
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "${var.cidr}"
}

resource "aws_network_acl_rule" "mobile-1" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1000
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "76.120.61.100/32"
}

resource "aws_network_acl_rule" "mobile-2" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1001
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "73.243.43.89/32"
}


resource "aws_network_acl_rule" "S3-1" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1002
  egress         = false
  protocol       = "6"
  rule_action    = "allow"
  cidr_block     = "52.92.48.0/22"
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "S3-2" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1003
  egress         = false
  protocol       = "6"
  rule_action    = "allow"
  cidr_block     = "52.219.20.0/22"
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "S3-3" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1004
  egress         = false
  protocol       = "6"
  rule_action    = "allow"
  cidr_block     = "52.219.24.0/21"
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "S3-4" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1005
  egress         = false
  protocol       = "6"
  rule_action    = "allow"
  cidr_block     = "54.231.232.0/21"
  from_port      = 1024
  to_port        = 65535
}
/*
resource "aws_network_acl_rule" "se-app-servers" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1006
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "172.26.40.0/28"
}

resource "aws_network_acl_rule" "se-10nets" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1007
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "10.31.0.0/16"
}

resource "aws_network_acl_rule" "mediator" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1008
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "10.248.29.128/25"
}

resource "aws_network_acl_rule" "some-others" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1009
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "172.26.43.128/25"
}


resource "aws_network_acl_rule" "mobile-3" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 1010
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "198.178.8.81/32"
}
resource "aws_network_acl_rule" "all-out" {
  network_acl_id = "${aws_network_acl.main.id}"
  rule_number    = 2000
  egress         = true
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
}
*/
