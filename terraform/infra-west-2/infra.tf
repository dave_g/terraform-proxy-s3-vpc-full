locals { // todo : get the ids created by cloud groups
  region     = "us-west-2" //todo : change to correct region
  vpc_id = "vpc-sdl345584030"
  cidr = "10.146.0.0/16" 
  subnet1 = "subnet-052ea03f488893632" 
  subnet2 = "subnet-0389cf873f632d5cd" 
  auto_scaling_subnets = ["subnet-052ea03f488893632"] //use to bounce instances between subnets/az.
  load_balancer_subnets = ["subnet-052ea03f488893632", "subnet-0389cf873f632d5cd"]
  route_tables = ["rtb-047521d8e409aa3a2"]

  service_name = "com.amazonaws.us-west-2.s3" //todo : change to correct region
  key_name = "key_west_2" //todo : change to correct region
  auto_scale_type = "t2.micro"//"t2.micro" //todo : change to m5.large

  //todo : how do insert a custom squid.conf? can .conf be downloaded from s3?
  auto_scale_ami = "ami-asd34567" //squid server v 4

  delete_protect = "false" // for the load balancer todo : set to true for production
  //cidr_blocks = ["76. 120.61.100/32", "73.243.43.89/32","172.26.40.0/28","10.31.0.0/16","172.26.43.128/25","10.248.29.128/25", "10.146.0.0/16", "198.178.8.81/32"] //security group local cidrs
  cidr_blocks = ["0.0.0.0/0"] //security group local cidrs
  s3_cidr_blocks = ["52.92.48.0/22", "52.219.20.0/22","52.219.24.0/21",  "54.231.232.0/21"] //security group local cidrs
  // todo : re-evaluate tag passing after 0.12 terraform is released that will Support count in resource fields #7034 - https://github.com/hashicorp/terraform/issues/7034
  tag_app_name = "S3-VPC"
  tag_env = "development"
  tag_lb_name = "Proxy-LB"
  tag_ec2_name = "S3-Proxy-Server"

}

/*
stuff for testing

//create an internet gatway if not created already.
resource "aws_internet_gateway" "gw" {
  vpc_id = "${local.vpc_id}"

  tags {
    Name = "main"
  }
}

*/

//for testing exteral internet access to the vpc. spin up a testing proxy. normally the proxies do not have public ip address
resource "aws_instance" "example" {
  ami           = "${local.auto_scale_ami}"
  instance_type = "t2.micro"
  key_name = "${local.key_name}"
  subnet_id = "${local.auto_scaling_subnets[0]}"
}

resource "aws_eip" "ip" {
  instance = "${aws_instance.example.id}"
}


resource "aws_route" "igw_route" {
  route_table_id               = "${local.route_tables[0]}"
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id = "igw-0323fdf9f43de3143"
}
/* END TESTING STUFF*/

provider "aws" {
  region = "${local.region}"
  profile = "ttdAdmin"
  allowed_account_ids = ["123456789"]
  shared_credentials_file = "~/.aws/credentials"
}

module "acls" {
  source = "../acls"
  subnet1 = "${local.subnet1}"
  subnet2 = "${local.subnet2}"
  vpc_id = "${local.vpc_id}"
}

//local cidr block all protocol rules
module "acl_cidr_rules" {
  source = "../acl_rules"
  rule_number_seed = 100
  cidr_blocks = "${local.cidr_blocks}"
  network_acl_id = "${module.acls.acl_id}"
}

//s3 cidr block rules
module "acl_s3_rules" {
  source = "../acl_rules"
  cidr_blocks = "${local.s3_cidr_blocks}"
  network_acl_id = "${module.acls.acl_id}"
  rule_number_seed = 200
  protocol = "6"
  from_port = "1024"
  to_port = "65535"
}

//all outbound
module "acl_outbound_rules" {
  source = "../acl_rules"
  cidr_blocks = ["0.0.0.0/0"]
  network_acl_id = "${module.acls.acl_id}"
  rule_number_seed = 2000
  protocol = "-1"
  egress = "true"
}

//todo : need sgs for 3128/80/443/22/icmp
module "sgs" {
  source = "../securityGroups"
  vpc_id = "${local.vpc_id}"
  ingress_cidr_blocks = "${local.cidr_blocks}"
  egress_cidr_blocks = ["0.0.0.0/0"]

  tag_app_name = "${local.tag_app_name}"
  tag_env = "${local.tag_env}"
  tag_name = "local-access"
  name = "local-access"
  description = "allows local addresses access to the vpc."

}

module "loadBalancer" {
  source = "../loadBalancer"
  load_balancer_subnets = "${local.load_balancer_subnets}"
  auto_scaling_subnets = "${local.auto_scaling_subnets}"
  delete_protect = "${local.delete_protect}"
  vpc_id = "${local.vpc_id}"
  auto_scale_ami = "${local.auto_scale_ami}"
  auto_scale_type = "${local.auto_scale_type}"
  security_group = "${module.sgs.id}" //get the security group id created in sgs module
  key_name = "${local.key_name}"
  // todo : re-evaluate after 0.12 terraform is released that will Support count in resource fields #7034 - https://github.com/hashicorp/terraform/issues/7034
  ec2_tags = ["${
    list(
      map("key", "ApplicationName", "value", "${local.tag_app_name}", "propagate_at_launch", true),
      map("key", "Name", "value", "${local.tag_ec2_name}", "propagate_at_launch", true),
      map("key", "ApplicationEnvironment", "value", "${local.tag_env}", "propagate_at_launch", true)
    )
  }"]
  tag_app_name = "${local.tag_app_name}"
  tag_env = "${local.tag_env}"
  tag_name = "${local.tag_lb_name}"

}

module "endPoint" {
  source = "../endpoint"
  vpc_id = "${local.vpc_id}"
  route_tables = "${local.route_tables}"
  service_name = "${local.service_name}"
}

output "load-balancer-id" {
  value = "${module.loadBalancer.load-balancer-id}"
}

output "load-balancer-dns" {
  value = "${module.loadBalancer.load-balancer-dns}"
}
