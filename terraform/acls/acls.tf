variable vpc_id {}
variable subnet1 {}
variable subnet2 {}


resource "aws_network_acl" "main" {
  vpc_id = "${var.vpc_id}"
  subnet_ids = ["${var.subnet1}","${var.subnet2}"]
}

output "acl_id" {
  value = "${aws_network_acl.main.id}"
}
